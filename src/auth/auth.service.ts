import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { Account } from 'src/account/entities/account.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Account) private AccountRepository: Repository<Account>){}


  async login(username :string , password : string) {
    console.log(username , password);
    const account = await this.AccountRepository.findOne({where : {user_name : username , user_password : password} ,relations : ['customer']})
    console.log(account)
    if (!account) {
      throw new UnauthorizedException();
    }

    if (account && password== account.user_password  ){
      return {
        account: account
      };
    }else return 

    
  }

}
