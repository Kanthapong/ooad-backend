import { Controller, Get, Post, Body, Patch, Param, Delete ,Request} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}


  @Post()
  findAccount(@Request() req) {
    console.log(req.body)
    return this.authService.login(req.body.user_name, req.body.user_password);
  }


}
