import { Injectable } from '@nestjs/common';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import { Payment } from './entities/payment.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'src/order/entities/order.entity';
import { OrderService } from 'src/order/order.service';

@Injectable()
export class PaymentService {
  constructor(
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    private orderService : OrderService

  ){}
  async create(createPaymentDto: CreatePaymentDto) {
    const newPayment = new Payment();
    const order = await this.orderRepository.findOne({where : {order_id : createPaymentDto.orderId}})
    newPayment.payment_image = createPaymentDto.payment_image;
    newPayment.payment_type = "QrCode"
    newPayment.payment_status = "รอยืนยันชำระเงิน"
    const payment = await this.paymentRepository.save(newPayment)
    order.payment = payment;
    return this.orderRepository.save(order);
  }

  findAll() {
    return this.paymentRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} payment`;
  }

  update(id: number, updatePaymentDto: UpdatePaymentDto) {
    return `This action updates a #${id} payment`;
  }

  remove(id: number) {
    return `This action removes a #${id} payment`;
  }
}
