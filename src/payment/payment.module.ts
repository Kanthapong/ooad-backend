import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { Order } from 'src/order/entities/order.entity';
import { Payment } from './entities/payment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderService } from 'src/order/order.service';
import { OrderController } from 'src/order/order.controller';
import { Customer } from 'src/customer/entities/customer.entity';
import { Product } from 'src/product/entities/product.entity';
import { OrderItem } from 'src/order/entities/order-item';
import { Address } from 'src/address/entities/address.entity';
import { AddressService } from 'src/address/address.service';

@Module({
  imports: [TypeOrmModule.forFeature([Payment,Order,Customer,Product,OrderItem,Address])],
  controllers: [PaymentController,],
  providers: [PaymentService,OrderService,AddressService]
})
export class PaymentModule {}
