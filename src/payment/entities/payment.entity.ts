import { Order } from "src/order/entities/order.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Payment {
    @PrimaryGeneratedColumn()
    payment_id : number

    @Column()
    payment_type : String

    @Column()
    payment_status : String

    @Column({nullable : true})
    payment_credit_name : String

    @Column({nullable : true})
    payment_credit_number : number

    @Column({nullable : true})
    payment_expiration_date : Date

    @Column()
    payment_image : String

    @OneToMany(() => Order, (order) => order.payment)
    orders : Order[];

    @CreateDateColumn()
    createdDate: Date;
  
    @UpdateDateColumn()
    updatedDate: Date;
  
    @DeleteDateColumn()
    deletedDate: Date;
}
