export class CreatePaymentDto {
    payment_type : String
    payment_credit_name : String
    payment_credit_number : number
    payment_expiration_date : String
    payment_status : String;
    payment_image : String
    orderId : number;
    
}
