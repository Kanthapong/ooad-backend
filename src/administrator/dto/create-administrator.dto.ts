import { IsNotEmpty, IsNumber, Length } from "class-validator";

export class CreateAdministratorDto {

    @IsNotEmpty()
    @Length(5,128)
    admin_name : string

    @IsNotEmpty()
    @Length(128)
    admin_email : string

    @IsNotEmpty()
    admin_role : string

    @IsNotEmpty()
    @Length(10)
    admin_tel : string

    @IsNotEmpty()
    admin_start_date : Date

    @IsNumber()
    admin_accountId: number;

    @IsNumber()
    admin_addressId: number;
}
