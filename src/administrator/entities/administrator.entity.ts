import { Account } from "src/account/entities/account.entity";
import { Address } from "src/address/entities/address.entity";
import { Category } from "src/category/entities/category.entity";
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToOne } from "typeorm";

@Entity()
export class Administrator {
    @PrimaryGeneratedColumn()
    admin_id : number;

    @Column({length: '128'})
    admin_name : string;

    @Column({length: '128'})
    admin_email : string;

    @Column()
    admin_role : string;

    @Column({length: '10'})
    admin_tel : string

    @Column()
    admin_start_date : Date

    @Column()
    admin_addressId: number;

    @Column()
    admin_accountId: number;

    @OneToOne(() => Account, (account) => account.administrator)
    account: Account;

    @OneToOne(() => Address, (address) => address.administrator)
    address: Address;



    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  
    @DeleteDateColumn()
    deletedAt: Date;
}
