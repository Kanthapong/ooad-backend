import { Injectable } from '@nestjs/common';
import { CreateAdministratorDto } from './dto/create-administrator.dto';
import { UpdateAdministratorDto } from './dto/update-administrator.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Administrator } from './entities/administrator.entity';
import { Repository } from 'typeorm';
import { Account } from 'src/account/entities/account.entity';
import { Address } from 'src/address/entities/address.entity';

@Injectable()
export class AdministratorService {
  constructor(@InjectRepository(Account) private AccountRepository : Repository<Account>,
              @InjectRepository(Administrator) private AdministratorRepository : Repository<Administrator>,
              @InjectRepository(Address) private AddressRepository : Repository<Address>
              ){}
  async create(createAdministratorDto: CreateAdministratorDto) {
    const newAdmin = new Administrator();
    const account = await this.AccountRepository.findOne({
      where: {
        account_id: createAdministratorDto.admin_accountId,
      },
    });
    newAdmin.account = account;
    newAdmin.admin_name = createAdministratorDto.admin_name;
    newAdmin.admin_email = createAdministratorDto.admin_email;
    newAdmin.admin_role = createAdministratorDto.admin_role;
    newAdmin.admin_tel = createAdministratorDto.admin_tel;
    newAdmin.admin_start_date = createAdministratorDto.admin_start_date;
    newAdmin.admin_accountId = createAdministratorDto.admin_accountId;
    return "this.AccountRepository.save(newAdmin)";
  }

  async findAll() {
    return this.AdministratorRepository.find();
  }

  findOneById(id: number) {
    return this.AdministratorRepository.findOneBy({admin_id : id});
  }

  findByAccount(id: number) {
    return this.AdministratorRepository.findBy({admin_accountId : id});
  }

  findByAddress(id: number) {
    return this.AdministratorRepository.findBy({admin_addressId : id})
  }

  update(id: number, updateAdministratorDto: UpdateAdministratorDto) {
    return `This action updates a #${id} administrator`;
  }

  remove(id: number) {
    return `This action removes a #${id} administrator`;
  }
}
