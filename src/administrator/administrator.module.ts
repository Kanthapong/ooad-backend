import { Module } from '@nestjs/common';
import { AdministratorService } from './administrator.service';
import { AdministratorController } from './administrator.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Administrator } from './entities/administrator.entity';
import { Account } from 'src/account/entities/account.entity';
import { Address } from 'src/address/entities/address.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Administrator, Account, Address])],
  controllers: [AdministratorController],
  providers: [AdministratorService],
  exports: [TypeOrmModule]
})
export class AdministratorModule {}
