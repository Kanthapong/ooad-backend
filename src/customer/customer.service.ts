import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Account } from 'src/account/entities/account.entity';

@Injectable()
export class CustomerService {
  getCustomer() {
    throw new Error('Method not implemented.');
  }
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  async create(createUserDto: CreateCustomerDto) {
    const newCustomer = new Customer();
    newCustomer.customer_name = createUserDto.customer_name
    newCustomer.email = createUserDto.email;
    newCustomer.tel = createUserDto.tel;
    return this.customerRepository.save(newCustomer);
  }

  findAll() {
    return this.customerRepository.find();
  }

  findOne(id: number) {
    return this.customerRepository.findOne({relations:['account'] ,where: {customer_id : id}});
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customerRepository.findOneBy({ customer_id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customerRepository.save(updatedCustomer);
  }

  // async update(id: number, updateCustomerDto: UpdateCustomerDto) {
  //   const product = await this.customerRepository.findOneBy({ customer_id : id });
  //   return `This action updates a #${id} customer`;
  // }

  remove(id: number) {
    return `This action removes a #${id} customer`;
  }
}

// import { Injectable, NotFoundException } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Repository } from 'typeorm';
// import { CreateCustomerDto } from './dto/create-customer.dto';
// import { UpdateCustomerDto } from './dto/update-customer.dto';
// import { Customer } from './entities/customer.entity';

// @Injectable()
// export class CustomerService {
//   constructor(
//     @InjectRepository(Customer)
//     private CustomerRepository: Repository<Customer>,
//   ) {}

//   create(createCustomerDto: CreateCustomerDto) {
//     return this.CustomerRepository.save(createCustomerDto);
//   }

//   findAll() {
//     return this.CustomerRepository.find({});
//   }

//   async findOne(id: number) {
//     const customer = await this.CustomerRepository.findOne({
//       where: { id: id },
//       relations: ['orders'],
//     });
//     if (!customer) {
//       throw new NotFoundException();
//     }
//     return customer;
//   }



//   async remove(id: number) {
//     const customer = await this.CustomerRepository.findOneBy({ id: id });
//     if (!customer) {
//       throw new NotFoundException();
//     }
//     return this.CustomerRepository.softRemove(customer);
//   }
// }
