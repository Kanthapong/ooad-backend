import { IsNotEmpty, Length, IsEmail, Matches } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(3, 32)
  customer_name: string;

  @IsNotEmpty()
  // @IsEmail()
  email: string;

  // @IsNotEmpty()
  // @Matches(
  //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  // )
  // password: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  
}
