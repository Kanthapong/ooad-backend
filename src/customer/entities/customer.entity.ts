import { Account } from 'src/account/entities/account.entity';
import { Address } from 'src/address/entities/address.entity';
import { Order } from 'src/order/entities/order.entity';
import { PrimaryGeneratedColumn, Entity, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';
@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  customer_id: number;

  @Column({ length: '50' })
  customer_name: string;

  @Column({  length: '64' })
  email: string;


  @Column()
  tel: string;

  @OneToMany(() => Address, (address) => address.customer)
  address : Address[];


  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];

  @OneToOne(() => Account, (account) => account.customer)
  account :Account;
}
