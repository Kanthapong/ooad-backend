import { Order } from "src/order/entities/order.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";


@Entity()
export class Shipment {
    @PrimaryGeneratedColumn()
    shipment_id : number;

    @Column()
    shipment_tracking_no : String;

    @Column()
    shipment_status : String;

    @Column()
    shipment_type : String;

    @Column({default : 0})
    shipment_cost : number;

    @OneToMany(() => Order, (order) => order.shipment)
    orders : Order[];

    @CreateDateColumn()
    createdDate: Date;
  
    @UpdateDateColumn()
    updatedDate: Date;
  
    @DeleteDateColumn()
    deletedDate: Date;


}
