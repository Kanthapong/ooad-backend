export class CreateShipmentDto {
    shipment_tracking_no : String;
    shipment_status : String;
    shipment_type : String;
    shipment_cost : number;
    order_id : number
}
