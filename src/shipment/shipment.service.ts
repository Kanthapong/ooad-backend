import { Injectable } from '@nestjs/common';
import { CreateShipmentDto } from './dto/create-shipment.dto';
import { UpdateShipmentDto } from './dto/update-shipment.dto';
import { Order } from 'src/order/entities/order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Shipment } from './entities/shipment.entity';

@Injectable()
export class ShipmentService {
  constructor(
    @InjectRepository(Shipment)
    private shipmentRepository: Repository<Shipment>,
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
  ){}
  async create(createShipmentDto: CreateShipmentDto) {
    const order = await this.ordersRepository.findOneBy({order_id : createShipmentDto.order_id})
    const newShipment = new Shipment();
    newShipment.shipment_tracking_no = createShipmentDto.shipment_tracking_no;
    newShipment.shipment_type = createShipmentDto.shipment_type;
    newShipment.shipment_status = 'กำลังจัดส่ง'
    const shipment = await this.shipmentRepository.save(newShipment);
    order.shipment = shipment;
    order.order_status = 'กำลังจัดส่ง'
    
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.shipmentRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} shipment`;
  }
  async shipmentSucc(id :number){
    const shipment = await this.shipmentRepository.findOneBy({shipment_id : id})
    shipment.shipment_status = 'จัดส่งสำเร็จ'
    return this.shipmentRepository.save(shipment)
  }

  update(id: number, updateShipmentDto: UpdateShipmentDto) {
    return `This action updates a #${id} shipment`;
  }

  remove(id: number) {
    return `This action removes a #${id} shipment`;
  }
}
