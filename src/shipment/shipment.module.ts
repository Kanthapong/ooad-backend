import { Module } from '@nestjs/common';
import { ShipmentService } from './shipment.service';
import { ShipmentController } from './shipment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Shipment } from './entities/shipment.entity';
import { Order } from 'src/order/entities/order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Shipment,Order])],
  controllers: [ShipmentController],
  providers: [ShipmentService],
  exports: [TypeOrmModule],
})
export class ShipmentModule {}
