import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';
export class CreateCategoryDto {
  @IsNotEmpty()
  @Length(1)
  category_name: string;
}
