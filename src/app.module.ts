import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from './product/product.module';
import { Product } from './product/entities/product.entity';
import { DataSource } from 'typeorm';
import { ProductController } from './product/product.controller';
import { ProductService } from './product/product.service';
import { ScheduleModule } from '@nestjs/schedule';

import { CategoryModule } from './category/category.module';
import { CategoryController } from './category/category.controller';
import { CategoryService } from './category/category.service';
import { Category } from './category/entities/category.entity';
import { CustomerModule } from './customer/customer.module';
import { CustomerController } from './customer/customer.controller';
import { CustomerService } from './customer/customer.service';
import { Customer } from './customer/entities/customer.entity';
import { AccountModule } from './account/account.module';
import { Account } from './account/entities/account.entity';
import { AccountController } from './account/account.controller';
import { AccountService } from './account/account.service';
import { AddressModule } from './address/address.module';
import { Address } from './address/entities/address.entity';
import { AddressController } from './address/address.controller';
import { AddressService } from './address/address.service';
import { SupporterModule } from './supporter/supporter.module';
import { AdministratorController } from './administrator/administrator.controller';
import { Administrator } from './administrator/entities/administrator.entity';
import { AdministratorModule } from './administrator/administrator.module';
import { AdministratorService } from './administrator/administrator.service';
import { AuthModule } from './auth/auth.module';
import { OrderModule } from './order/order.module';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/order-item';
import { OrderController } from './order/order.controller';
import { OrderService } from './order/order.service';
import { PaymentModule } from './payment/payment.module';
import { Payment } from './payment/entities/payment.entity';
import { ShipmentModule } from './shipment/shipment.module';
import { Shipment } from './shipment/entities/shipment.entity';
import { PaymentService } from './payment/payment.service';
import { ShipmentController } from './shipment/shipment.controller';
import { ShipmentService } from './shipment/shipment.service';

require('dotenv').config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: `${process.env.DB_IP}`,
      port: 3306,
      username: `${process.env.DB_USERNAME}`,
      password: `${process.env.DB_PASSWORD}`,
      database: `${process.env.DB_DB}`,
      entities: [Product, Category, Customer, Account, Address ,Administrator, Order, OrderItem,Payment,Shipment],
      synchronize: true,
    }),
    ProductModule,
    ScheduleModule.forRoot(),
    CategoryModule,
    CustomerModule,
    AccountModule,
    AddressModule,
    AddressModule,
    AdministratorModule,
    AuthModule,
    OrderModule,
    PaymentModule,
    ShipmentModule
  ],
  controllers: [
    AppController,
    ProductController,
    CategoryController,
    CustomerController,
    AccountController,
    AddressController,
    AdministratorController,
    OrderController,
    ShipmentController
  ],
  providers: [
    AppService,
    ProductService,
    CategoryService,
    CustomerService,
    AccountService,
    AddressService,
    AdministratorService,
    OrderService,
    PaymentService,
    ShipmentService
  ],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
