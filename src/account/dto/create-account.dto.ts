import { IsNotEmpty, Length } from 'class-validator';
import { CreateCustomerDto } from 'src/customer/dto/create-customer.dto';

export class CreateAccountDto {
  @IsNotEmpty()
  @Length(5, 128)
  user_name: string;

  @IsNotEmpty()
  @Length(5)
  user_password: string;

  @IsNotEmpty()
  user_role: string;

  @IsNotEmpty()
  customer: CreateCustomerDto
}
