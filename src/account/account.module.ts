import { Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './entities/account.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { CustomerService } from 'src/customer/customer.service';
import { CustomerController } from 'src/customer/customer.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Account,Customer])],
  controllers: [AccountController,CustomerController],
  providers: [AccountService,CustomerService],
  exports: [TypeOrmModule],
})
export class AccountModule {}
