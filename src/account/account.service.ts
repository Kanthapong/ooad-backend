import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from './entities/account.entity';
import { Repository } from 'typeorm';
import { Administrator } from 'src/administrator/entities/administrator.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { CustomerService } from 'src/customer/customer.service';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account) private AccountRepository: Repository<Account>,
    @InjectRepository(Customer) private CustomerRepository: Repository<Customer>,
    private readonly CustomerService: CustomerService
  ) {}
  async create(createAccountDto: CreateAccountDto) {
    const customer = await this.CustomerService.create(createAccountDto.customer)
    const newAccount = new Account();
    newAccount.user_name = createAccountDto.user_name;
    newAccount.user_password = createAccountDto.user_password;
    newAccount.user_role = createAccountDto.user_role;
    newAccount.customer = customer;
 
    const account = await this.AccountRepository.save(newAccount)
    return account ;
  }

  findAll() {
    return this.AccountRepository.find({relations:['customer']});
  }

  findOneById(id: number) {
    return this.AccountRepository.findOne({ where:{account_id : id} , relations: ['customer']});
  }

  async update(id: number, updateCustomerDto: UpdateAccountDto) {
    const account = await this.AccountRepository.findOneBy({ account_id: id });
    if (!account) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...account, ...updateCustomerDto };
    return this.AccountRepository.save(updatedCustomer);
  }

  remove(id: number) {
    return `This action removes a #${id} account`;
  }
}
