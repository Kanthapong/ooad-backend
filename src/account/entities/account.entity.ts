import { Administrator } from "src/administrator/entities/administrator.entity";
import { Customer } from "src/customer/entities/customer.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToOne, PrimaryColumn, JoinColumn } from "typeorm";

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  account_id: number;

  @PrimaryColumn()
  @Column({ length: '128' })
  user_name: string;

  @Column()
  user_password: string;

  @Column()
  user_role: string;

  @OneToOne(() => Administrator, (administrator) => administrator.account)
  administrator: Administrator;

  @OneToOne(() => Customer, (customer) => customer.account)
  @JoinColumn()
  customer: Customer;

  @CreateDateColumn()
  createdAt: Date;
  
  @UpdateDateColumn()
  updatedAt: Date;
  
  @DeleteDateColumn()
  deletedAt: Date;
}
