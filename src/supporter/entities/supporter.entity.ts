import { Account } from "src/account/entities/account.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Supporter {
    @PrimaryGeneratedColumn()
    support_id : number;

    // @OneToOne(() => Account, (account) => account.administrator)

    @Column({length: '128'})
    support_name : string;
}
