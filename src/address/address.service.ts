import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Address } from './entities/address.entity';
import { CreateAddressDto } from './dto/create-address.dto';
import { CreateOrderDto } from 'src/order/dto/create-order.dto';
import { Customer } from 'src/customer/entities/customer.entity';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  async create(createStoreDto: CreateOrderDto) {
    const newAddress = new Address();
    const customer = await this.customerRepository.findOneBy({
      customer_id: createStoreDto.customerId,
    });
    newAddress.address_addDetail = createStoreDto.address.address_addDetail;
    newAddress.address_country = createStoreDto.address.address_country;
    newAddress.address_district = createStoreDto.address.address_district;
    newAddress.address_firstname = createStoreDto.address.address_firstname;
    newAddress.address_lastname = createStoreDto.address.address_lastname;
    newAddress.address_postal_code = createStoreDto.address.address_postal_code;
    newAddress.address_province = createStoreDto.address.address_province;
    newAddress.address_tel = createStoreDto.address.address_tel;
    newAddress.customer = customer;
    
    return this.addressRepository.save(newAddress);
  }

  findAll() {
    return this.addressRepository.find();
  }
}
