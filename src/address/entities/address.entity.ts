import { Administrator } from 'src/administrator/entities/administrator.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Order } from 'src/order/entities/order.entity';
import { PrimaryGeneratedColumn, Entity, Column, OneToOne, ManyToOne, OneToMany } from 'typeorm';
@Entity()
export class Address {
  @PrimaryGeneratedColumn()
  address_id: number;

  @Column({ length: '50' })
  address_firstname: string;

  @Column({ length: '50' })
  address_lastname: string;

  @Column({ length: '128' })
  address_addDetail: string;

  @Column({ length: '128' })
  address_district: string;

  @Column({ length: '128' })
  address_province: string;

  @Column({ length: '128' })
  address_postal_code: string;

  @Column({ length: '128' })
  address_country: string;

  @Column()
  address_tel: string;

  @OneToOne(() => Administrator, (administrator) => administrator.address)
  administrator: Administrator;

  @ManyToOne(() => Customer, (customer) => customer.address)
  customer : Customer;

  @OneToMany(() => Order, (order) => order.address)
  orders : Order[];
}
