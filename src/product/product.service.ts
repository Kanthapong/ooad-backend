import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { Category } from 'src/category/entities/category.entity';

@Injectable()
export class ProductService {
  constructor(@InjectRepository(Product) private ProductRepository : Repository<Product> ,
              @InjectRepository(Category) private CategoryRepository : Repository<Category> 
  ){}
 async create(createProductDto: CreateProductDto,file : Express.Multer.File[]) {

    const newProduct = new Product();
    const category = await this.CategoryRepository.findOne({
      where: {
        category_id: createProductDto.product_categoryId,
      },
    });
    newProduct.product_img_url = [];
    newProduct.category = category;
    newProduct.product_name = createProductDto.product_name;
    newProduct.product_price = createProductDto.product_price;
    newProduct.product_size = createProductDto.product_size;
    newProduct.product_description = createProductDto.product_description;
    newProduct.product_color = createProductDto.product_color;
    newProduct.product_amout  = createProductDto.product_amout;
    file.forEach((item) => {newProduct.product_img_url = [...newProduct.product_img_url,item.filename]})
    
    newProduct.product_categoryId = createProductDto.product_categoryId;
    return this.ProductRepository.save(newProduct);
  }

  async findAll() {
    return this.ProductRepository.find();
  }

  findOneById(id: number) {
    return this.ProductRepository.findOneBy({ product_id: id });
  }
  findByCategory(id: number) {
    return this.ProductRepository.findBy({ product_categoryId: id });
  }

//  async update(id: number, updateProductDto: UpdateProductDto) {
//     const product = await this.ProductRepository.findOneBy({ product_id : id });
//     if (!product) {
//       throw new NotFoundException();
//     }
//     const updatedProduct = { ...product, ...updateProductDto };
//     return this.ProductRepository.save(updatedProduct);
//   }

  async remove(id: number) {
    const product = await this.ProductRepository.findOneBy({ product_id :id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.ProductRepository.softRemove(product);
  }

  }

