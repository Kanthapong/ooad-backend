import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile, Res, UploadedFiles } from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { AnyFilesInterceptor, FileFieldsInterceptor, FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}


  // @Post()
  // @UseInterceptors(FilesInterceptor('file',10,{
  //       storage: diskStorage({
  //         destination: './File/images',
  //         filename: (req, file, cb) => {
  //           const name = uuidv4();
  //           return cb(null, name + extname(file.originalname));
  //         },
  //       }),
  //     }) )
  @Post()
async create(@Body() createProductDto: CreateProductDto,@UploadedFiles() file: Express.Multer.File[]) {
    return this.productService.create(createProductDto,file);
  }


  
  @Get()
  findAll() {
    return this.productService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productService.findOneById(+id);
  }
  @Get('category/:id')
  findByCategory(@Param('id') id: string) {
    return this.productService.findByCategory(+id);
  }

  // @Get(':image/:imageFile')
  // async getImageByFileName(
  //   @Param('imageFile') imageFile: string,
  //   @Res() res: Response,
  // ) {
  //   res.sendFile(imageFile, { root: './File/images' });
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
  //   return this.productService.update(+id, updateProductDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productService.remove(+id);
  }
}
