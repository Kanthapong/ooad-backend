import { Category } from 'src/category/entities/category.entity';
import { OrderItem } from 'src/order/entities/order-item';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  product_id: number;

  @Column({ length: '50' })
  product_name: string;

  @Column({type:'simple-array',  nullable: true})
  product_size : string;

  @Column({ type: 'float' })
  product_price: number;

  @Column()
  product_color: string;

  @Column()
  product_description: string;

  @Column()
  product_amout: number;

  @Column()
  product_rating: number;

  @Column()
  product_categoryId: number;

  @Column({type:'simple-array',  nullable: true})
  product_img_url: string[];

  @ManyToOne(() => Category, (category) => category.products)
  category: Category;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
