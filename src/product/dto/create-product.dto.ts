import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(5, 10)
  product_name: string;

  @IsNotEmpty()
  @Length(1)
  product_size : string;

  @IsNotEmpty()
  product_price: number;

  @IsNotEmpty()
  @Length(1)
  product_color: string;

  @IsNotEmpty()
  product_description: string;

  @IsNumber()
  @Min(0)
  product_amout: number;

  product_rating: number;

  product_img_url: string;

  @IsNumber()
  product_categoryId: number;
}
