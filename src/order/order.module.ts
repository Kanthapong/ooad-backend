import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/order-item';
import { Product } from 'src/product/entities/product.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Payment } from 'src/payment/entities/payment.entity';
import { Shipment } from 'src/shipment/entities/shipment.entity';
import { Address } from 'src/address/entities/address.entity';
import { AddressService } from 'src/address/address.service';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Product, Customer,Payment,Shipment,Address])],
  controllers: [OrderController],
  providers: [OrderService,AddressService],
  exports: [TypeOrmModule],
})
export class OrderModule {}
