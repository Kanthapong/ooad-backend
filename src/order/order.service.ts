import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customer/entities/customer.entity';
import { Product } from 'src/product/entities/product.entity';
import { Repository, Like, IsNull, Not } from 'typeorm';

import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { Address } from 'src/address/entities/address.entity';
import { AddressService } from 'src/address/address.service';
import { Payment } from 'src/payment/entities/payment.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
    private addressService : AddressService,
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const order: Order = new Order();
    const customer = await this.customerRepository.findOneBy({
      customer_id: createOrderDto.customerId,
    });

    if(createOrderDto.address.address_id == 0){
      const address = await this.addressService.create(createOrderDto)
      order.address = address
      console.log(address)
    }

    order.customer = customer;

    order.order_sum_amount = 0;
    order.order_total_price = 0;
    order.order_status = 'รอการตรวจสอบชำระเงิน'
    await this.ordersRepository.save(order);

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.product_amout;
      orderItem.product = await this.productsRepository.findOneBy({
        product_id: od.product_id,
      });
      orderItem.name = orderItem.product.product_name;
      orderItem.price = orderItem.product.product_price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order;
      orderItem.product_size = od.product_size;
      orderItem.product_img_url = orderItem.product.product_img_url
      await this.orderItemsRepository.save(orderItem);
      order.order_sum_amount = order.order_sum_amount + orderItem.amount;
      order.order_total_price = order.order_total_price + orderItem.total;
    }
    await this.ordersRepository.save(order);
    return await this.ordersRepository.findOne({
      where: { order_id: order.order_id },
      relations: ['orderItems','customer','payment','address'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItems','customer','payment','address'],
    });
  }

  findByCusId(id :number) {
    return this.ordersRepository.find({where : {customer : {customer_id :id}},
      relations: ['orderItems','customer','payment','address','shipment'],
    });
  }


  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { order_id: id },
      relations: ['shipment','payment','orderItems','customer','payment','address'],
      
    });
  }

/// ของ payment
  findPayment() {
    return this.ordersRepository.find({
      where:{ payment :{payment_status : "รอยืนยันชำระเงิน"} ,order_status : 'รอการตรวจสอบชำระเงิน'},
      relations: ['orderItems','customer','payment','address']
      
    });
  }

//จัดเตรียมออเดอร์
findPeparOrder() {
  return this.ordersRepository.find({
    where:{ payment :{payment_status : "รอยืนยันชำระเงิน"} ,order_status : 'กำลังจัดเตรียมสินค้า'},
    relations: ['orderItems','customer','payment','address']
    
  });
}

findOrderShipping() {
  return this.ordersRepository.find({
    where:{ payment :{payment_status : "รอยืนยันชำระเงิน"} ,order_status : 'กำลังจัดส่ง'},
    relations: ['orderItems','customer','payment','address']
    
  });
}
findOrderComplete() {
  return this.ordersRepository.find({
    where:{ payment :{payment_status : "รอยืนยันชำระเงิน"} ,order_status : 'จัดส่งสำเร็จ'},
    relations: ['orderItems','customer','payment','address']
    
  });
}


/// ของ shipment
  findShipment() {
    return this.ordersRepository.find({
      where:{ order_status : 'กำลังจัดเตรียมสินค้า' , shipment : IsNull() },
      relations: ['shipment','payment','orderItems','customer','payment','address'],
      
    });
  }
  findOnShipment() {
    return this.ordersRepository.find({
      where:{ order_status : 'กำลังจัดส่ง' , shipment : Not(IsNull()) },
      relations: ['shipment','payment','orderItems','customer','payment','address'],
      
    });
  }
  async verification(id : number){
    const order =  await this.ordersRepository.findOneBy({order_id:id})
    order.order_status = 'กำลังจัดเตรียมสินค้า'
    return this.ordersRepository.save(order);
  }
  async PreparOrder(id : number){
    const order =  await this.ordersRepository.findOneBy({order_id:id})
    order.order_status = 'กำลังจัดส่ง'
    return this.ordersRepository.save(order);
  }
  async OrderComplete(id : number){
    const order =  await this.ordersRepository.findOneBy({order_id:id})
    order.order_status = 'จัดส่งสำเร็จ'
    return this.ordersRepository.save(order);
  }
  



  async update(id: number, updateOrderDto: UpdateOrderDto) {
        const order = await this.ordersRepository.findOneBy({ order_id : id });
        if (!order) {
          throw new NotFoundException();
        }
        const updatedProduct = { ...order, ...updateOrderDto };
        return this.ordersRepository.save(updatedProduct);
      }
    

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ order_id: id });
    return this.ordersRepository.remove(order);
  }

  findByName(id: number) {
    return this.ordersRepository.find({
      where: { order_id: Like(id) },
    });
  }
}
