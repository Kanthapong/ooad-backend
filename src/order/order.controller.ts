import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,Request,
  UseInterceptors,
  UploadedFile
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';


@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}


  
  @Post()
  @UseInterceptors(FileInterceptor('file',{
        storage: diskStorage({
          destination: './File/images',
          filename: (req, file, cb) => {
            const name = uuidv4();
            return cb(null, name + extname(file.originalname));
          },
        }),
      }) )
  create(@Body() createOrderDto :CreateOrderDto,@UploadedFile() file: Express.Multer.File) {
    console.log(createOrderDto)
    return this.orderService.create(createOrderDto);
  }

  @Get()
  findAll() {
    return this.orderService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderService.findOne(+id);
  }

  @Get(':id/customer')
  findByCusId(@Param('id') id: string) {
    return this.orderService.findByCusId(+id);
  }

/// ของ payment
  @Get('payment/order')
  findOnePayment() {
    return this.orderService.findPayment();
  }

/// ของ shipment
  @Get('order/sp')
  findOneShipment() {
    return this.orderService.findShipment() ;
  }
  @Get('OnShipment/order')
  findOShipment() {
    return this.orderService.findOnShipment() ;
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(+id, updateOrderDto);
  }
  @Patch(':id/verify')
  verifiyPayment(@Param('id')id :string){
    return this.orderService.verification(+id)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }
}
