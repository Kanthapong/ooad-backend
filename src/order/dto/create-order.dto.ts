import { IsNotEmpty } from 'class-validator';
import { Address } from 'src/address/entities/address.entity';
import { Payment } from 'src/payment/entities/payment.entity';

class CreatedOrderItemDto {
  @IsNotEmpty()
  product_id: number;
  @IsNotEmpty()
  product_price: number;
  @IsNotEmpty()
  product_amout: number;
  @IsNotEmpty()
  product_size : string[0];
}
export class CreateOrderDto {
  customerId: number;


  // payment: Payment;

  address: Address;


  // @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
