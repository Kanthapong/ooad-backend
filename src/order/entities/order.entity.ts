import { Customer } from 'src/customer/entities/customer.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { OrderItem } from './order-item';
import { Address } from 'src/address/entities/address.entity';
import { Payment } from 'src/payment/entities/payment.entity';
import { Shipment } from 'src/shipment/entities/shipment.entity';
@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  order_id: number;

  @Column()
  order_total_price: number;

  @Column()
  order_sum_amount: number;

  @Column()
  order_status : String;


  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer;

  @ManyToOne(() => Address, (address) => address.orders)
  address: Address;

  @ManyToOne(() => Payment, (payment) => payment.orders)
  payment: Payment;

  @ManyToOne(() => Shipment, (shipment) => shipment.orders)
  shipment: Shipment;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  // @ManyToOne(() => User, (user) => user.orders)
  // user: User;
}
